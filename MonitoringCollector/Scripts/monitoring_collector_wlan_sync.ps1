[Net.ServicePointManager]::SecurityProtocol = "tls12, tls11";

$ErrorActionPreference = "Stop"

$ScriptDir = Split-Path -parent $MyInvocation.MyCommand.Path
$env:PSModulePath = $env:PSModulePath + ";" + $ScriptDir + "\..\Modules"

$myFQDNwmi=(Get-WmiObject win32_computersystem).DNSHostName+"."+(Get-WmiObject win32_computersystem).Domain
$myFQDN=$myFQDNwmi.ToLower()

Import-Module DolphinWlanSync

$WlanExportFolder = New-TemporaryDirectory

Export-WLAN -XmlDirectory $WlanExportFolder.FullName

$WlanArray = @()

Get-ChildItem $WlanExportFolder -Filter *.xml | 
Foreach-Object {
    $content = Get-Content $_.FullName
    $WlanArray = @($content)
}

$requestBody = @{
    deployment_id = (Get-Item "HKLM:\Software\System-Lights.com\SLagent").GetValue('DeploymentId')
    deployment_pw = (Get-Item "HKLM:\Software\System-Lights.com\SLagent").GetValue('DeploymentPw')
    fqdn = $myFQDN
    wlan_export = $WlanArray
}

$json_data = $requestBody | ConvertTo-Json

Invoke-RestMethod -Method 'Put' -Uri "https://api.system-lights.com/monitoring/api/wlan-sync?format=json" -ContentType "application/json; charset=utf-8" -Body $json_data
