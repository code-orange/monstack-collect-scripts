@echo off
timeout /T 120 /nobreak
"C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe" -command "& {Set-ExecutionPolicy -ExecutionPolicy Unrestricted -Force}"
"C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe" -NoLogo -NonInteractive -InputFormat None -NoProfile -File "C:\Program Files (x86)\System-Lights.com\SLagent\MonitoringCollector\Scripts\monitoring_collector_hardware.ps1" run
"C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe" -NoLogo -NonInteractive -InputFormat None -NoProfile -File "C:\Program Files (x86)\System-Lights.com\SLagent\MonitoringCollector\Scripts\monitoring_collector_apps.ps1" run
"C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe" -NoLogo -NonInteractive -InputFormat None -NoProfile -File "C:\Program Files (x86)\System-Lights.com\SLagent\MonitoringCollector\Scripts\monitoring_collector_services.ps1" run
"C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe" -NoLogo -NonInteractive -InputFormat None -NoProfile -File "C:\Program Files (x86)\System-Lights.com\SLagent\MonitoringCollector\Scripts\monitoring_collector_wlan_sync.ps1" run
exit %ERRORLEVEL%
