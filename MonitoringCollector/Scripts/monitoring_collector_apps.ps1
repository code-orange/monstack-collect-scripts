[Net.ServicePointManager]::SecurityProtocol = "tls12, tls11";

$ErrorActionPreference = "Stop"

$myFQDNwmi=(Get-WmiObject win32_computersystem).DNSHostName+"."+(Get-WmiObject win32_computersystem).Domain
$myFQDN=$myFQDNwmi.ToLower()

$deployment_id = (Get-Item "HKLM:\Software\System-Lights.com\SLagent").GetValue('DeploymentId')
$deployment_pw = (Get-Item "HKLM:\Software\System-Lights.com\SLagent").GetValue('DeploymentPw')

$apps = Get-WmiObject Win32_product | Select-Object @{ expression={$_.name}; label='app_name' }

$requestBody = @{
    deployment_id = $deployment_id
    deployment_pw = $deployment_pw
    fqdn = $myFQDN
    apps = $apps
}

$headers = @{
    'Authorization' = 'ApiKey {0}:{1}' -f $deployment_id,$deployment_pw
}

$json_data = $requestBody | ConvertTo-Json

Invoke-RestMethod -Method 'Put' -Uri "https://api.system-lights.com/monitoring/api/apps?format=json" -Headers $headers -ContentType "application/json; charset=utf-8" -Body $json_data
