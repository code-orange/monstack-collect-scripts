[Net.ServicePointManager]::SecurityProtocol = "tls12, tls11";

$ErrorActionPreference = "Stop"

$ScriptDir = Split-Path -parent $MyInvocation.MyCommand.Path
$env:PSModulePath = $env:PSModulePath + ";" + $ScriptDir + "\..\Modules"

$myFQDNwmi=(Get-WmiObject win32_computersystem).DNSHostName+"."+(Get-WmiObject win32_computersystem).Domain
$myFQDN=$myFQDNwmi.ToLower()

$OSwmi = Get-WmiObject -Class Win32_OperatingSystem;
$OSver = $OSwmi.Caption

$deployment_id = (Get-Item "HKLM:\Software\System-Lights.com\SLagent").GetValue('DeploymentId')
$deployment_pw = (Get-Item "HKLM:\Software\System-Lights.com\SLagent").GetValue('DeploymentPw')

$computer_info = @{
    ComputerInfo = Get-ComputerInfo
    Win32_ComputerSystem = Get-WmiObject Win32_ComputerSystem
    Win32_BaseBoard = Get-WmiObject Win32_BaseBoard
    Win32_Bios = Get-WmiObject Win32_Bios
    Win32_Processor = Get-WmiObject Win32_Processor
    Win32_PhysicalMemory = Get-WmiObject Win32_PhysicalMemory
    Win32_DiskDrive = Get-WmiObject Win32_DiskDrive
    Win32_LogicalDisk = Get-WmiObject Win32_LogicalDisk
    Win32_NetworkAdapter = Get-WmiObject Win32_NetworkAdapter
    Win32_OperatingSystem = Get-WmiObject Win32_OperatingSystem
}

$requestBody = @{
    deployment_id = $deployment_id
    deployment_pw = $deployment_pw
    fqdn = $myFQDN
    os_family = 'windows'
    os_version = $OSver
    computer_info = $computer_info
}

$headers = @{
    'Authorization' = 'ApiKey {0}:{1}' -f $deployment_id,$deployment_pw
}

$ticket = Invoke-RestMethod -Method 'Post' -Uri https://api.system-lights.com/monitoring/api/hosts?format=json -Headers $headers -Body $requestBody

Import-Module Icinga2Agent

$icinga = Icinga2AgentModule `
                -AgentName       $ticket.cn `
                -TransformHostname       1 `
                -Ticket          $ticket.ticket `
                -InstallAgentVersion   $ticket.version `
                -ParentZone      'master' `
                -ParentEndpoints 'mon-master01.system-lights.com', 'mon-master02.system-lights.com' `
                -EndpointsConfig 'mon-master01.system-lights.com;5665', 'mon-master02.system-lights.com;5665' `
                -CAServer        'mon-master01.system-lights.com' `
                -IcingaServiceUser     'LocalSystem' `
                -AllowUpdates `
                -AcceptConfig $true `
                -IcingaEnableDebugLog $false `
                -IcingaDisableLogging $true `
                -FlushApiDirectory `
                -ForceCertificateGeneration `
                -FullUninstallation `
                -InstallNSClient `
                -NSClientAddDefaults;
                
$icinga.uninstall();

exit $icinga.install();